package com.example;

import com.example.model.WageLine;
import com.example.model.Workday;
import com.example.model.Workshift;
import com.example.rules.Summary;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

/**
 * Created by wikbmi on 20.6.2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class HourListReaderTest {

    @Autowired
    HourListReader hourListReader;

    @Test
    public void testWorkdayConstructor() {
        Workday wd = new Workday("John Nullpointer", "1", "1.2.2001", "00:15", "1:15");
        wd.initShifts();
        assertEquals(Integer.valueOf(1), wd.getEmployeeId());
        assertEquals(new Workshift(LocalDateTime.of(2001, 2, 1, 0, 15), LocalDateTime.of(2001, 2, 1, 1, 15)), wd.getShifts().get(0));
    }

    @Test
    public void testReadFile() throws IOException {

        Resource fileRes = new ClassPathResource("HourList201403.csv");
        List<Workday> workdays = hourListReader.readFile(fileRes);
        assertEquals("Wrong number of days", 62, workdays.size());
        List<WageLine> res = Summary.getSummary(workdays);
        WageLine expected = new WageLine(1, "Janet Java", 173, 691.875, 12, 17, 60, 91.875);
        expected.setEmployeeName("Janet Java");
        assertEquals("Wrong first line", expected, res.get(0));
        assertThat(res, Matchers.hasSize(4));
    }

    @Test(expected = IOException.class)
    public void testReadInvalidFile() throws IOException {
        Resource fileRes = new ClassPathResource("dummyfile.txt");
        List<Workday> workdays = hourListReader.readFile(fileRes);
        assertEquals("Wrong number of days", 62, workdays.size());
        List<WageLine> res = Summary.getSummary(workdays);
    }


    @Test
    public void testWageCalculation() throws IOException {
        Resource fileRes = new ClassPathResource("HourListTest.csv");
        List<Workday> workdays = hourListReader.readFile(fileRes);
        assertEquals("Wrong number of days", 4, workdays.size());

        List<WageLine> res = Summary.getSummary(workdays);
        assertThat(res, Matchers.hasSize(4));

        //employeeId, employeeName, totalWorkHours, netWage, eveningWorkHours, overtimeWorkHours, eveningWorkWage, overtimeWorkWage
        WageLine expected1 = new WageLine(1, "Jane Doe", 3.5, 13.13, 0, 0, 0, 0);
        WageLine expected2 = new WageLine(2, "John Doe", 8, 31.25, 1, 0, 5, 0);
        WageLine expected3 = new WageLine(3, "Adam Ascii", 8.75, 33.52, 0, 0.75, 0, 3.52);
        WageLine expected4 = new WageLine(4, "John Nullpointer", 31, 194.38, 1, 23, 5, 163.13);

        assertThat("", res, Matchers.contains(expected1, expected2, expected3, expected4));
    }
}
