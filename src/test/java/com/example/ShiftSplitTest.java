package com.example;

import com.example.model.Workday;
import org.junit.Test;

import static com.example.rules.Worktime.checkWorkTimeLimits;
import static org.junit.Assert.assertEquals;

/**
 * Created by wikbmi on 20.6.2016.
 */
public class ShiftSplitTest {

    String date = "6.6.2016";

    @Test
    public void testInitShiftsNormal1() {
        Workday wd = new Workday("John", "1", date, "8:00", "10:00");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(1, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsLate1() {
        Workday wd = new Workday("John", "1", date, "17:00", "19:00");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(2, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsLate2() {
        Workday wd = new Workday("John", "1", date, "17:00", "9:00");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(7, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsEarly1() {
        Workday wd = new Workday("John", "1", date, "3:00", "7:00");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(2, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsLate3() {
        Workday wd = new Workday("John", "1", date, "23:00", "1:30");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(2, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsLate4() {
        Workday wd = new Workday("John", "1", date, "23:00", "7:00");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(3, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsEarly2() {
        Workday wd = new Workday("John", "1", date, "5:00", "19:00");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(6, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsLong1() {
        Workday wd = new Workday("John", "1", date, "0:00", "23:45");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(6, wd.getShifts().size());
    }

    @Test
    public void testInitShiftsLong2() {
        Workday wd = new Workday("John", "1", date, "0:00", "11:45");
        wd.initShifts();
        checkWorkTimeLimits(wd);
        assertEquals(4, wd.getShifts().size());
    }

}
