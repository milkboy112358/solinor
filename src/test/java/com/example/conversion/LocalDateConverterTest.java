package com.example.conversion;

import com.opencsv.exceptions.CsvConstraintViolationException;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;
import org.junit.Test;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created by wikbmi on 22.6.2016.
 *
 */
public class LocalDateConverterTest {

    @Test(expected = DateTimeParseException.class)
    public void testConvertException() throws Exception {
        LocalDateConverter converter = new LocalDateConverter();
        Object res = converter.convert("Foobar");
        fail("Should throw exception");
    }

    @Test
    public void testConvert() throws CsvRequiredFieldEmptyException, CsvConstraintViolationException, CsvDataTypeMismatchException {
        LocalDateConverter converter = new LocalDateConverter();
        LocalDate res = (LocalDate) converter.convert("31.1.2016");
        assertEquals("Wrong date returned", LocalDate.of(2016,1,31), res);
    }
}
