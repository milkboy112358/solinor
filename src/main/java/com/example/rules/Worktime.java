package com.example.rules;

import com.example.Constants;
import com.example.model.Workday;
import com.example.model.Workshift;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.Constants.EVENING_WORK_END;
import static com.example.Constants.EVENING_WORK_START;
import static com.example.rules.Worktime.WorkType.Evening;
import static com.example.rules.Worktime.WorkType.Normal;
import static com.example.rules.Worktime.WorkType.Overtime100;
import static com.example.rules.Worktime.WorkType.Overtime25;
import static com.example.rules.Worktime.WorkType.Overtime50;

public class Worktime {

    public enum WorkType {
        Normal, Evening, Overtime25, Overtime50, Overtime100
    }

    public static boolean isEveningWork(Workshift shift) {
        return shift.getStart().toLocalTime().isBefore(EVENING_WORK_END)
                || shift.getEnd().toLocalTime().isAfter(EVENING_WORK_START)
                || shift.getEnd().toLocalTime().equals(LocalTime.MIDNIGHT);
    }


    public static void checkWorkTimeLimits(Workday wd) {
        //Split at overtime boundaries
        //8, 10, 12 hours
        long totalTimeMinutes = 0;
        List<Workshift> tmp = new ArrayList<>();
        int i = 0;
        while(i < wd.getShifts().size()) {
            tmp.clear();

            Workshift shift = wd.getShifts().get(i++);

            long duration = shift.getDuration();
            long remainingMinutes;

            if (totalTimeMinutes < 8 * 60) {
                remainingMinutes = 8 * 60 - totalTimeMinutes;
                if (remainingMinutes < duration) {
                    tmp.add(shift.splitAt(shift.getStart().plusMinutes(remainingMinutes)));
                }
                totalTimeMinutes += shift.getDuration();
                shift.setWorkType(isEveningWork(shift) ? Evening : Normal);
            } else if (totalTimeMinutes < 10 * 60) {
                remainingMinutes = 10 * 60 - totalTimeMinutes;
                if (remainingMinutes < duration) {
                    tmp.add(shift.splitAt(shift.getStart().plusMinutes(remainingMinutes)));
                }
                totalTimeMinutes += shift.getDuration();
                shift.setWorkType(Overtime25);
            } else if (totalTimeMinutes < 12 * 60) {
                remainingMinutes = 12 * 60 - totalTimeMinutes;
                if (remainingMinutes < duration) {
                    tmp.add(shift.splitAt(shift.getStart().plusMinutes(remainingMinutes)));
                }
                totalTimeMinutes += shift.getDuration();
                shift.setWorkType(Overtime50);
            } else {
                totalTimeMinutes += shift.getDuration();
                shift.setWorkType(Overtime100);
            }

            updateWage(shift);

            if(!tmp.isEmpty()) {
                wd.getShifts().addAll(tmp);
                wd.sortShifts();
            }
        }
    }

    private static void updateWage(Workshift shift) {
        double modifier;
        switch (shift.getWorkType()) {
            case Evening:
                modifier = (Constants.HOURLY_WAGE + Constants.EVENING_SHIFT_ADDITION) / Constants.HOURLY_WAGE;
                break;
            case Overtime25:
                modifier = 1.25;
                break;
            case Overtime50:
                modifier = 1.5;
                break;
            case Overtime100:
                modifier = 2;
                break;
            case Normal:
            default:
                modifier = 1;
                break;
        }
        shift.setWage(modifier * Constants.HOURLY_WAGE * shift.getDuration() / 60);
    }


}
