package com.example.rules;

import com.example.model.WageLine;
import com.example.model.Workday;
import com.example.model.Workshift;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Summary {

    private static final Logger LOGGER = LoggerFactory.getLogger(Summary.class);

    public static List<WageLine> getSummary(List<Workday> workdays) {

        //Get workday entries grouped by employee id
        Map<Integer, List<Workday>> groupedWorkdays = workdays.stream().collect(Collectors.groupingBy(Workday::getEmployeeId));

        List<WageLine> wageLines = new ArrayList<>();
        for(Integer employeeId : groupedWorkdays.keySet()) {
            WageLine line = new WageLine(employeeId);
            //also fetch employee name from first entry
            line.setEmployeeName(groupedWorkdays.get(employeeId).get(0).getEmployeeName());
            groupedWorkdays.get(employeeId).stream().forEach(workday -> {
                for(Workshift shift : workday.getShifts()) {
                    line.addShift(shift);
                }
            });
            LOGGER.trace("{}", line);
            wageLines.add(line);
        }

        LOGGER.debug("Calculated wages: {}", wageLines);

        return wageLines;
    }
}
