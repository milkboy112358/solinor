package com.example.web;

import com.example.HourListReader;
import com.example.model.WageLine;
import com.example.model.Workday;
import com.example.rules.Summary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Controller
public class WebController {

    @Autowired
    HourListReader hourListReader;

    @RequestMapping("/")
    public String index() {
        return "upload";
    }

    @RequestMapping("/about")
    public String about() {
        return "about";
    }

    /**
     * For gettting any previously calculated data
     * @param model
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/summary", method = RequestMethod.GET)
    public String upload(Model model, HttpSession session) throws IOException {
        List<WageLine> summary;
        LocalDate dateStart, dateEnd;
        dateStart = (LocalDate) session.getAttribute("dateStart");
        dateEnd = (LocalDate) session.getAttribute("dateEnd");
        summary = (List<WageLine>) session.getAttribute("summary");
        model.addAttribute("dateStart", dateStart);
        model.addAttribute("dateEnd", dateEnd);
        model.addAttribute("wages", summary);

        return "summary";
    }

    /**
     * Post new hour list
     * @param hourlist
     * @param model
     * @param session
     * @return
     * @throws IOException
     */
    @RequestMapping(value = "/summary", method = RequestMethod.POST)
    public String upload(@RequestParam("hourlist") MultipartFile hourlist, Model model, HttpSession session) throws IOException {
        List<WageLine> summary = null;
        LocalDate dateStart, dateEnd;
        File tmpFile = File.createTempFile("hourlist", ".tmp");
        try {
            hourlist.transferTo(tmpFile);
            if(hourlist.isEmpty()) {
                throw new IOException("Empty or no file uploaded");
            }
            List<Workday> wd = hourListReader.readFile(new FileSystemResource(tmpFile));
            dateStart = wd.get(0).getDate();
            dateEnd = wd.get(wd.size() - 1).getDate();
            summary = Summary.getSummary(wd);
            session.setAttribute("summary", summary);
        } finally {
            tmpFile.delete();
        }
        session.setAttribute("dateStart", dateStart);
        session.setAttribute("dateEnd", dateEnd);
        session.setAttribute("summary", summary);

        model.addAttribute("dateStart", dateStart);
        model.addAttribute("dateEnd", dateEnd);
        model.addAttribute("wages", summary);
        return "summary";
    }

    @RequestMapping(value = "/detail/{id}", produces = "application/json; charset=UTF-8")
    /*Use to return JSON
    @ResponseBody
    public WageLine getWageDetail(@PathVariable Integer id, HttpSession session) {
        return getDetail(session, id);
    }*/
    //But we use a fragment
    public String getWageDetail(@PathVariable Integer id, HttpSession session, Model model) {

        model.addAttribute("detail", getDetail(session, id));
        return "summary :: detail";
    }

    private WageLine getDetail(HttpSession session, Integer id) {
        List<WageLine> wageLines = (List<WageLine>) session.getAttribute("summary");
        if(wageLines == null) return null;
        Optional<WageLine> details = wageLines.stream().filter(wageLine -> wageLine.getEmployeeId() == id).findFirst();

        return details.isPresent() ? details.get() : null;
    }

    @RequestMapping(value = "/login")
    public String login() {
        return "login";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }
}
