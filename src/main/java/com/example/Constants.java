package com.example;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class Constants {

    public static final double HOURLY_WAGE = 3.75;
    public static final double EVENING_SHIFT_ADDITION = 1.25;

    public static final LocalTime EVENING_WORK_START = LocalTime.of(18,0);
    public static final LocalTime EVENING_WORK_END = LocalTime.of(6,0);

    public static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("d.M.y");
    public static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("H:mm");

}
