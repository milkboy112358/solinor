package com.example;

import com.example.model.Workday;
import com.example.model.WorkdayList;
import com.example.rules.Worktime;
import com.opencsv.CSVReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.HeaderColumnNameMappingStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.validation.Validator;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class HourListReader {

    @Autowired
    Validator validator;

    public List<Workday> readFile(Resource resource) throws IOException {
        ArrayList<Workday> res = new ArrayList<>();
        CsvToBean<Workday> csv = new CsvToBean<>();
        HeaderColumnNameMappingStrategy<Workday> strategy = new HeaderColumnNameMappingStrategy<>();
        strategy.setType(Workday.class);


        WorkdayList<Workday> list = new WorkdayList<>();
        list.setWorkdays(csv.parse(strategy, new CSVReader(new FileReader(resource.getFile()))));

        Set<?> validationResult = validator.validate(list);

        if(!validationResult.isEmpty()) {
            Object ex = validationResult.iterator().next();
            throw new IOException("At least one validation failed: " + ex.toString());
        }

        ConcurrentHashMap<String, Workday> workdays = new ConcurrentHashMap<>();

        list.getWorkdays().parallelStream().forEach(workday -> {
            workday.initShifts();
            workdays.merge(workday.getDate() + ":" + workday.getEmployeeId(), workday, (workday1, workday2) -> {
                workday1.getShifts().addAll(workday2.getShifts());
                workday1.sortShifts();
                return workday1;
            });
        });

        workdays.values().parallelStream().forEach(Worktime::checkWorkTimeLimits);
        res.addAll(workdays.values());
        res.sort((o1, o2) -> {
            int d = o1.getDate().compareTo(o2.getDate());
            return d == 0 ? o1.getEmployeeId().compareTo(o2.getEmployeeId()) : d;
        });

        return res;
    }
}
