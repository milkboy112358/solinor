package com.example.model;

import com.example.rules.Worktime;

import java.time.Duration;
import java.time.LocalDateTime;

/**
 * Created by wikbmi on 19.6.2016.
 */
public class Workshift {
    public Workshift splitAt(LocalDateTime localDateTime) {
        Workshift newWorkshift = new Workshift(localDateTime, end);
        this.end = localDateTime;
        return newWorkshift;
    }

    public long getDuration() {
        return Duration.between(start, end).toMinutes();
    }

    private LocalDateTime start;
    private LocalDateTime end;
    private Worktime.WorkType workType;
    private double wage;

    public Workshift(LocalDateTime start, LocalDateTime end) {
        this.start = start;
        this.end = end;
    }

    public Workshift() {

    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }

    public Worktime.WorkType getWorkType() {
        return workType;
    }

    public void setWorkType(Worktime.WorkType workType) {
        this.workType = workType;
    }

    public double getWage() {
        return wage;
    }

    public void setWage(double wage) {
        this.wage = wage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Workshift workshift = (Workshift) o;

        if (Double.compare(workshift.wage, wage) != 0) return false;
        if (start != null ? !start.equals(workshift.start) : workshift.start != null) return false;
        if (end != null ? !end.equals(workshift.end) : workshift.end != null) return false;
        return workType == workshift.workType;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = start != null ? start.hashCode() : 0;
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (workType != null ? workType.hashCode() : 0);
        temp = Double.doubleToLongBits(wage);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
