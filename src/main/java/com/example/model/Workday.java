package com.example.model;

import com.example.conversion.LocalDateConverter;
import com.example.conversion.LocalTimeConverter;
import com.opencsv.bean.CsvBindByName;
import com.opencsv.bean.CsvCustomBindByName;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.example.Constants.DATE_FORMATTER;
import static com.example.Constants.TIME_FORMATTER;

public class Workday {
    @CsvBindByName(column = "Person ID", required = true, locale = "fi-FI")
    @NotNull
    private Integer employeeId;
    @NotNull
    @CsvBindByName(column = "Person Name", required = true, locale = "fi-FI")
    private String employeeName;
    @NotNull
    @CsvCustomBindByName(column = "Date", converter = LocalDateConverter.class)
    private LocalDate date;
    @NotNull
    @CsvCustomBindByName(column = "Start", converter = LocalTimeConverter.class)
    private LocalTime start;
    @NotNull
    @CsvCustomBindByName(column = "End", converter = LocalTimeConverter.class)
    private LocalTime end;
    private ArrayList<Workshift> shifts;
    private float salary;



    static Set<LocalTime> splits;
    static {
        splits = new HashSet<>();
        splits.add(LocalTime.of(6, 0));
        splits.add(LocalTime.of(18, 0));
    }


    public Workday() {
    }

    //testing only?
    public Workday(String employeeName, String employeeId, String date, String start, String end) {
        this.employeeName = employeeName;
        this.employeeId = Integer.valueOf(employeeId);
        this.date = LocalDate.parse(date, DATE_FORMATTER);
        this.start = LocalTime.parse(start, TIME_FORMATTER);
        this.end = LocalTime.parse(end, TIME_FORMATTER);
    }

    public void initShifts() {
        if(shifts == null) {
            shifts  = new ArrayList<>();
            shifts.add(new Workshift(
                            this.date.atTime(start),
                            //End date may be after midnight
                            end.isBefore(start) ? this.date.atTime(end).plusDays(1) : this.date.atTime(end))
            );
            start = null; end = null;
        }

        //Split shifts that cross borders (evening work start+end, midnight)
        List<Workshift> tmp = new ArrayList<>();
        do {
            tmp.clear();
            for (Workshift i : shifts) {
                if(i.getStart().toLocalDate().isBefore(i.getEnd().toLocalDate()) && i.getEnd().toLocalTime().compareTo(LocalTime.MIDNIGHT) != 0) {
                    tmp.add(i.splitAt(i.getEnd().toLocalDate().atStartOfDay()));
                }
                for (LocalTime s : splits) {
                    LocalDate tmpDate = i.getStart().toLocalDate();
                    if (i.getStart().isBefore(tmpDate.atTime(s))) {
                        if(i.getEnd().isAfter(tmpDate.atTime(s))) {
                            tmp.add(i.splitAt(tmpDate.atTime(s)));
                        }
                    }
                }
            }
            shifts.addAll(tmp);
        } while (!tmp.isEmpty());

        sortShifts();
    }

    public void sortShifts() {
        shifts.sort((o1, o2) -> o1.getStart().compareTo(o2.getStart()));
    }

    public Integer getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public List<Workshift> getShifts() {
        return shifts;
    }

    public void setShifts(ArrayList<Workshift> shifts) {
        this.shifts = shifts;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Workday workday = (Workday) o;

        if (employeeId != null ? !employeeId.equals(workday.employeeId) : workday.employeeId != null) return false;
        if (employeeName != null ? !employeeName.equals(workday.employeeName) : workday.employeeName != null)
            return false;
        if (date != null ? !date.equals(workday.date) : workday.date != null) return false;
        if (start != null ? !start.equals(workday.start) : workday.start != null) return false;
        if (end != null ? !end.equals(workday.end) : workday.end != null) return false;
        return !(shifts != null ? !shifts.equals(workday.shifts) : workday.shifts != null);

    }

    @Override
    public int hashCode() {
        int result = employeeId != null ? employeeId.hashCode() : 0;
        result = 31 * result + (employeeName != null ? employeeName.hashCode() : 0);
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (start != null ? start.hashCode() : 0);
        result = 31 * result + (end != null ? end.hashCode() : 0);
        result = 31 * result + (shifts != null ? shifts.hashCode() : 0);
        return result;
    }
}
