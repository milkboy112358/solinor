package com.example.model;

import java.math.BigDecimal;

public class WageLine {

    private double totalWorkHours;
    private int employeeId;
    private String employeeName;
    private double netWage;
    private double eveningWorkHours;
    private double overtimeWorkHours;
    private double eveningWorkWage;
    private double overtimeWorkWage;

    //Used in testing only
    public WageLine(int employeeId, String employeeName, double totalWorkHours, double netWage, double eveningWorkHours, double overtimeWorkHours, double eveningWorkWage, double overtimeWorkWage) {
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.totalWorkHours = totalWorkHours;
        this.netWage = netWage;
        this.eveningWorkHours = eveningWorkHours;
        this.overtimeWorkHours = overtimeWorkHours;
        this.eveningWorkWage = eveningWorkWage;
        this.overtimeWorkWage = overtimeWorkWage;
    }

    public WageLine(Integer employeeId) {
        this.employeeId = employeeId;
    }

    public void addShift(Workshift shift) {
        switch (shift.getWorkType()) {
            case Overtime100:
            case Overtime50:
            case Overtime25:
                overtimeWorkHours += shift.getDuration() / 60d;
                overtimeWorkWage += shift.getWage();
                break;
            case Evening:
                eveningWorkHours += shift.getDuration() / 60d;
                eveningWorkWage += shift.getWage();
        }
        netWage += shift.getWage();
        totalWorkHours += shift.getDuration() / 60d;
    }

    public double getTotalWorkHours() {
        return totalWorkHours;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public double getNetWage() {
        return new BigDecimal(netWage).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public double getEveningWorkHours() {
        return eveningWorkHours;
    }

    public double getOvertimeWorkHours() {
        return overtimeWorkHours;
    }

    public double getEveningWorkWage() {
        return new BigDecimal(eveningWorkWage).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public double getOvertimeWorkWage() {
        return new BigDecimal(overtimeWorkWage).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        WageLine wageLine = (WageLine) o;

        if (Double.compare(wageLine.getTotalWorkHours(), getTotalWorkHours()) != 0) return false;
        if (getEmployeeId() != wageLine.getEmployeeId()) return false;
        if (Double.compare(wageLine.getNetWage(), getNetWage()) != 0) return false;
        if (Double.compare(wageLine.getEveningWorkHours(), getEveningWorkHours()) != 0) return false;
        if (Double.compare(wageLine.getOvertimeWorkHours(), getOvertimeWorkHours()) != 0) return false;
        if (Double.compare(wageLine.getEveningWorkWage(), getEveningWorkWage()) != 0) return false;
        if (Double.compare(wageLine.getOvertimeWorkWage(), getOvertimeWorkWage()) != 0) return false;
        return !(getEmployeeName() != null ? !getEmployeeName().equals(wageLine.getEmployeeName()) : wageLine.getEmployeeName() != null);

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        temp = Double.doubleToLongBits(getTotalWorkHours());
        result = (int) (temp ^ (temp >>> 32));
        result = 31 * result + getEmployeeId();
        result = 31 * result + (getEmployeeName() != null ? getEmployeeName().hashCode() : 0);
        temp = Double.doubleToLongBits(getNetWage());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getEveningWorkHours());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getOvertimeWorkHours());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getEveningWorkWage());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(getOvertimeWorkWage());
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "WageLine{" +
                "totalWorkHours=" + totalWorkHours +
                ", employeeId=" + employeeId +
                ", employeeName='" + employeeName + '\'' +
                ", netWage=" + netWage +
                ", eveningWorkHours=" + eveningWorkHours +
                ", overtimeWorkHours=" + overtimeWorkHours +
                ", eveningWorkWage=" + eveningWorkWage +
                ", overtimeWorkWage=" + overtimeWorkWage +
                '}';
    }
}
