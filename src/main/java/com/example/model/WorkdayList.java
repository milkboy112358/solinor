package com.example.model;

import javax.validation.Valid;
import java.util.List;

/**
 * Wrapper for validating CSV entries
 * @param <Workday>
 */
public class WorkdayList<Workday> {
    @Valid
    List<Workday> workdays;

    public List<Workday> getWorkdays() {
        return workdays;
    }

    public void setWorkdays(List<Workday> workdays) {
        this.workdays = workdays;
    }
}
